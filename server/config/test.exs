use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :history_sink, HistorySinkWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :history_sink, HistorySink.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "historysink",
  password: "historysink",
  database: "history_sink_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
