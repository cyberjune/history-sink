defmodule HistorySinkWeb.PageController do
  use HistorySinkWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
